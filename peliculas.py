#!/usr/bin/env python
# -*- coding: utf-8 -*-

# archivo clase para main


class Peliculas():

    def get_titulo(self):
        return self.titulo

    def get_genero(self):
        return self.genero

    def get_pais(self):
        return self.pais

    def get_director(self):
        return self.director

    def get_review(self):
        return self.review

    def set_titulo(self, titulo):
            self.titulo = titulo

    def set_genero(self, genero):
        self.genero = genero

    def set_pais(self, pais):
        self.pais = pais

    def set_director(self, director):
        self.director = director

    def set_review(self, review):
        self.review = review
