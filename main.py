#!/usr/bin/env python
# -*- coding: utf-8 -*-

# se importa pandas, sistema y el archivo
import pandas as pd
import os
from peliculas import Peliculas


# función almacena datos llamando a csv
def datos():
    # primero se llama al data base con todos los datos
    archivos = pd.read_csv("IMDb_movies.csv")
    # se filtran las columnas que quermos usar
    new = archivos.filter(["title", "year", "genre", "country", "director",
                           "reviews_from_critics"])
    # se filtra por año = 2018
    movies_2018 = new[new["year"] == 2018]
    # se filtra nuevamente ya que el año es innecesario
    new_2 = movies_2018.filter(["title", "genre", "country", "director",
                                "reviews_from_critics"])
    # creamos un archivo csv para facilidad de uso
    new_2.to_csv("movies_2018.csv", index=False)
    # data finalizado es el nuevo csv
    data = pd.read_csv("movies_2018.csv")
    return data


# función de menú de busqueda
def busqueda():
    # ingresar valores válidos
    try:
        print(" -BUSCAR INFO POR:\n  1.Titulo\n  2.Género\n  3.País")
        buscar = int(input("  >> "))
        if buscar == 1:
            os.system('clear')
            # se llama a la función de busqueda de título
            busca = str(input(" -Ingrese título: "))
            buscar_titulo(busca, peliculas_list)
            # se llama a la función de busqueda de género
        elif buscar == 2:
            os.system('clear')
            busca = str(input(" -Ingrese Género: "))
            buscar_genero(busca, peliculas_list)
        # se llama a la función de busqueda de país
        elif buscar == 3:
            os.system('clear')
            busca = str(input(" -Ingrese País: "))
            buscar_pais(busca, peliculas_list)
        else:
            os.system('clear')
            print(" |INGRESE UNA OPCIÓN VÁLIDA|")
            busqueda()
    except ValueError:
        os.system('clear')
        print(" |INGRESE UNA OPCIÓN VÁLIDA|")
        busqueda()


# función que almacena los datos de la data base en la clase
def pelicula_objeto(data):
    peliculas_list = []
    for i, j in data.iterrows():
        # se crea el objeto
        peliculas = Peliculas()
        # se envían los datos de las columnas a la clase
        peliculas.set_titulo(j["title"].capitalize())
        peliculas.set_genero(j["genre"].split(","))
        peliculas.set_pais(str(j["country"]).split(","))
        peliculas.set_director(j["director"])
        peliculas.set_review(j["reviews_from_critics"])
        peliculas_list.append(peliculas)
        # se usa esto para que la lista de países quede en orden
        # y se puedan asociar a palabras con minúsculas
        pais = peliculas.get_pais()
        p = []
        for i in pais:
            i = i.strip()
            i = i.capitalize()
            p.append(i)
            peliculas.set_pais(p)
        # lo mismo con el género
        genero = peliculas.get_genero()
        gen = []
        for i in genero:
            i = i.strip()
            i = i.capitalize()
            gen.append(i)
            peliculas.set_genero(gen)
    # entrega una lista con todos los datos
    return peliculas_list


# función que busca título ingresado en los datos almacenados en la clase
def buscar_titulo(busca, peliculas_list):
    encuentra = 0
    # recorre la lista la lista y al encontrar datos similares
    # imprime toda la información del dato ingresado
    for i in peliculas_list:
        # capitalize para que considere minúsculas
        if busca.capitalize() in i.get_titulo():
            print(" -Película: ", i.get_titulo())
            print(" -Género: ", ', '.join(i.get_genero()))
            # join para cuando haya más de un dato y se puedan separar
            print(" -País: ", ', '.join(i.get_pais()))
            print(" -Director: ", i.get_director())
            print(" -Puntución de los criticos: ", i.get_review())
            print("=====================================================")
            encuentra += 1
    if encuentra == 0:
        print("No se encontró ninguna coincidencia con lo ingresado")
    print("=====================================================")
    menu()


# función buscar género ingresado entre los datos de la clase
def buscar_genero(busca, peliculas_list):
    encuentra = 0
    # recorre la lista la lista y al encontrar datos similares
    # imprime toda la información del dato ingresado
    for i in peliculas_list:
        # capitalize para que al ingresarse con minúsculas igual se considere
        if busca.capitalize() in i.get_genero():
            print(" -Película: ", i.get_titulo())
            # join para que haya una separación entre los datos,
            # solo si hay más de uno
            print(" -Género: ", ', '.join(i.get_genero()))
            print(" -País: ", ', '.join(i.get_pais()))
            print(" -Director: ", i.get_director())
            print(" -Puntución de los criticos: ", i.get_review())
            print("=====================================================")
            encuentra += 1
    if encuentra == 0:
        print("No se encontró ninguna coincidencia con lo ingresado")
    print("=====================================================")
    menu()


# función que busca el país ingresado entre los datos guardados
def buscar_pais(busca, peliculas_list):
    encuentra = 0
    # recorre la lista la lista y al encontrar datos similares
    # imprime toda la información del dato ingresado
    for i in peliculas_list:
        # capitalize para que al ingresarse con minúsculas igual se considere
        if busca.capitalize() in i.get_pais():
            print(" -Película: ", i.get_titulo())
            print(" -Género: ", ', '.join(i.get_genero()))
            print(" -País: ", ', '.join(i.get_pais()))
            print(" -Director: ", i.get_director())
            print(" -Puntución de los criticos: ", i.get_review())
            print("=====================================================")
            encuentra += 1
    # si no encuentra se muestra un mensaje
    if encuentra == 0:
        print("No se encontró ninguna coincidencia con lo ingresado")
    print("=====================================================")
    menu()


# función que imprime lista de titulos de las películas
def imprime(peliculas_list):
    print("   PELÍCULAS")
    print("   *********")
    num = 1
    # recorre la lista y llama a get para que imprima los datos
    for i in peliculas_list:
        print(num, i.get_titulo())
        num += 1


# función menú
def menu():
    # debe ingresar valores válidos
    try:
        print("   |Ingrese una opción|")
        print("  1.Ver lista\n  2.Buscar  película\n  3.Salir")
        opcion = int(input("  >> "))
        num = 1
        if opcion == 1:
            os.system('clear')
            # cuando se pide la lista de películas se llama a la
            # función que imprime todos los títulos
            imprime(peliculas_list)
            print("\n")
            menu()
        # opción 2 se llama a la funciónn de busqueda
        elif opcion == 2:
            os.system('clear')
            busqueda()
        # opción 3 termina el programa
        elif opcion == 3:
            os.system('clear')
            print("\n\n     -|ADIÓS|-")
            exit()
        else:
            os.system('clear')
            print(" |INGRESE UNA OPCIÓN VÁLIDA|")
            menu()
    except ValueError:
        os.system('clear')
        print(" |INGRESE UNA OPCIÓN VÁLIDA|")
        menu()

# Funcion principal
if __name__ == "__main__":
    # se llama la data que se utilizará
    data = datos()
    print("   BASE DE DATOS DE PELÍCULAS DEL AÑO 2018")
    print("   ***************************************")
    peliculas_list = pelicula_objeto(data)
    menu()
